function NewPhone(
  name,
  type,
  price,
  screen,
  backCamera,
  frontCamera,
  img,
  desc
) {
  this.name = name;
  this.type = type;
  this.price = price;
  this.screen = screen;
  this.backCamera = backCamera;
  this.frontCamera = frontCamera;
  this.img = img;
  this.desc = desc;
}
